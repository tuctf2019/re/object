# Object

Desc: `We've recovered this file but can't make much of it.`

Given files:

* run.o

Hints:

* Do you see a way to procedurally recover the password?

Flag: `TUCTF{c0n6r47ul4710n5_0n_br34k1n6_7h15_fl46}`
