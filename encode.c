#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

char *password;
int passlen;

void encode() {
	// easy print for copying later
	printf("int passlen = %d;\n", passlen);
	printf("char *password = \"");

	for (int i = 0; i < passlen; i++) {
 		unsigned char c = password[i];
		c <<= 1;
		c = ~c;
		c ^= 0xAA; // 0b10101010

		// Print out \x41
		printf("\\x%hhx", c);
	}
	printf("\";\n");
}

void readpass() {
	password = malloc(64);
	memset(password, 0, 64);
	int fd = open("./flag.txt", 0);

	// -1 b/c of the return character
	passlen = read(fd, password, 64)-1;
	// remove the return character
	password[passlen] = 0;

	close(fd);
	printf("OG: %s\n\n", password);
}

int main() {
	setvbuf(stdout, NULL, _IONBF, 20);
	setvbuf(stdin, NULL, _IONBF, 20);

	readpass();
	encode();

	return 0;
}
