#!/usr/bin/python
from pwn import *
from platform import architecture
from os import environ, getpid, listdir
from pyperclip import copy as pcpy
#___________________________________________________________________________________________________
local=False;ldp=False;pause=False;copy=False; # payload=''
rem =       ""
cmd =       "./run"
libcid =    ""
local =     True    # run the binary localy
#pause =     True    # pause the binary when running localy
#copy =      True   # Copy the PID of the binary to clipboard
#ldp =       True   # LD_PRELOAD the binary & switch libc-files
#___________________________________________________________________________________________________
if libcid is not '':
    libcdl = '/home/thpoons/desktop/libc-database/libs/' + libcid
    libcf =  libcdl+'/'+[f for f in listdir(libcdl) if f.startswith("libc.so")][0]
    ldf =    libcdl+'/'+[f for f in listdir(libcdl) if f.startswith("ld-linux")][0]
#___________________________________________________________________________________________________
elf =   ELF(cmd.split(' ')[0], False)
libc =  ELF(libcf if ldp else ([key for key in elf.libs.keys() if 'libc' in key][0]), False)
context.arch = 'i386' if ('32bit' in architecture(cmd.split(' ')[0])) else 'amd64'
#___________________________________________________________________________________________________
passlen = 44
# 1337 Prioritized keyspace
keyspace = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ{}!"#$%&\'()*+,-./:;<=>?@[\]^_`|~'

# "My password"
mp = list("A"*passlen)


# Loop through password
for i in range(passlen):
    for c in keyspace:
        # Spawn process
        p = process(cmd.split(' '));
        # Get new char
        mp[i] = c;

        p.sendline(''.join(mp))

        # Break on 'Correct Password'
        p.recvlines(2)
        if 'Incorrect' not in p.recvline():
            p.close()
            break

        # Get if we have the right char or not
        p.recvuntil('character: ')
        fail = int(p.recvuntil('\n').strip())
        print "fail: " + str(fail)
        if fail > i:
            p.close()
            break

        p.close()

#___________________________________________________________________________________________________
print '\n'*2
print 'flag: ' + ''.join(mp)
