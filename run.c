#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

int passlen = 44;
char *password = "\xfd\xff\xd3\xfd\xd9\xa3\x93\x35\x89\x39\xb1\x3d\x3b\xbf\x8d\x3d\x3b\x37\x35\x89\x3f\xeb\x35\x89\xeb\x91\xb1\x33\x3d\x83\x37\x89\x39\xeb\x3b\x85\x37\x3f\xeb\x99\x8d\x3d\x39\xaf";

int checkPassword(char *userpass) {
	int userlen = strlen(userpass);
	if (userlen != passlen) {
		puts("Close, but no flag");
		return 1;
	}

	// Loop char by char
	for (int i = 0; i < passlen; i++) {
		unsigned char a = userpass[i];
		unsigned char b = password[i];

		// Bad encoding
		a <<= 1;
		a = ~a;
		a ^= 0xaa;

		if (a != b) {
			printf("Incorrect password\nError at character: %d\n", i);
			return 1;
		}
	}
	puts("Correct Password!");
	return 0;
}

int main() {
	setvbuf(stdout, NULL, _IONBF, 20);
	setvbuf(stdin, NULL, _IONBF, 20);

	char *userpass = malloc(64);
	memset(userpass, 0, 64);

	// Get user input
	printf("Enter password:\n> ");
	// They shouldn't use spaces anyway
	scanf("%64s", userpass);
	printf("pass: %s\n", userpass);

    return checkPassword(userpass);
}
