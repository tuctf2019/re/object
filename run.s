	.file	"run.c"
	.text
	.globl	passlen
	.data
	.align 4
	.type	passlen, @object
	.size	passlen, 4
passlen:
	.long	44
	.globl	password
	.section	.rodata
	.align 8
.LC0:
	.string	"\375\377\323\375\331\243\2235\2119\261=;\277\215=;75\211?\3535\211\353\221\2613=\2037\2119\353;\2057?\353\231\215=9\257"
	.section	.data.rel.local,"aw"
	.align 8
	.type	password, @object
	.size	password, 8
password:
	.quad	.LC0
	.section	.rodata
.LC1:
	.string	"Close, but no flag"
	.align 8
.LC2:
	.string	"Incorrect password\nError at character: %d\n"
.LC3:
	.string	"Correct Password!"
	.text
	.globl	checkPassword
	.type	checkPassword, @function
checkPassword:
.LFB6:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, %rdi
	call	strlen@PLT
	movl	%eax, -4(%rbp)
	movl	passlen(%rip), %eax
	cmpl	%eax, -4(%rbp)
	je	.L2
	leaq	.LC1(%rip), %rdi
	call	puts@PLT
	movl	$1, %eax
	jmp	.L3
.L2:
	movl	$0, -8(%rbp)
	jmp	.L4
.L6:
	movl	-8(%rbp), %eax
	movslq	%eax, %rdx
	movq	-24(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -10(%rbp)
	movq	password(%rip), %rdx
	movl	-8(%rbp), %eax
	cltq
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -9(%rbp)
	salb	-10(%rbp)
	notb	-10(%rbp)
	xorb	$-86, -10(%rbp)
	movzbl	-10(%rbp), %eax
	cmpb	-9(%rbp), %al
	je	.L5
	movl	-8(%rbp), %eax
	movl	%eax, %esi
	leaq	.LC2(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$1, %eax
	jmp	.L3
.L5:
	addl	$1, -8(%rbp)
.L4:
	movl	passlen(%rip), %eax
	cmpl	%eax, -8(%rbp)
	jl	.L6
	leaq	.LC3(%rip), %rdi
	call	puts@PLT
	movl	$0, %eax
.L3:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	checkPassword, .-checkPassword
	.section	.rodata
.LC4:
	.string	"Enter password:\n> "
.LC5:
	.string	"%64s"
.LC6:
	.string	"pass: %s\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB7:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	stdout(%rip), %rax
	movl	$20, %ecx
	movl	$2, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	setvbuf@PLT
	movq	stdin(%rip), %rax
	movl	$20, %ecx
	movl	$2, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	setvbuf@PLT
	movl	$64, %edi
	call	malloc@PLT
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	$64, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	.LC4(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movq	-8(%rbp), %rax
	movq	%rax, %rsi
	leaq	.LC5(%rip), %rdi
	movl	$0, %eax
	call	__isoc99_scanf@PLT
	movq	-8(%rbp), %rax
	movq	%rax, %rsi
	leaq	.LC6(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	checkPassword
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 9.2.1-9ubuntu2) 9.2.1 20191008"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
