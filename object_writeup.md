# Object Writeup

## Setup

Compiling goes through many states before becoming a binary. One of those states is the `object` code. TLDR: It's all the executable instructions that aren't in a form that can be executed.

They are given this object-code file and need to go forth.

## Solution

1. You can compile any mid-state of the compilation process. Running `gcc run.o` will produce a valid binary to poke at.
1. The binary will ask for a password (length = 44) and check that by doing screwing with it and comparing it against other garbage.
1. There are 2 solutions. The intended one is as follows:
1. Understand that it is checking character by character for the password and telling you which charcter is wrong. You can then build a quick brute-force script and run through all keyspaces to extract the password
1. the alternate solution is to actualy understand how it's messing with the password (it's easy intentionally) and reverse that process on the password. This is left intentionally based on how they want to approach it.
